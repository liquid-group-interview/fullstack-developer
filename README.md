# Fullstack Developer Technical Test

As a user, I want to see an checkout page after completed the shopping in merchant website, so that i can make the payment to the merchant

**Acceptance Criteria**

- Order call should be made with liquid system to send the redirect URL to merchant.
- once customer redirected, the browser should show the hosted payment checkout page
- User should be able to see the order items with the transaction amount, order id and order Reference.
- An form should be shown to users to enter their credit card details, expiry date and cvv (Refer to mockup)
- An session id should be used while redirecting the customers to make sure its unique to the transaction

**Transaction Flow**

- Customer purchase Item A, Item B in the merchant e commerce site and add it to the shopping cart.
- Once done click on the checkout, merchant should trigger an API request to liquid to get the redirect url as below

Request:

```json
{
    "service_type": "ecom",
    "amount": "1500",
    "order_id": "Order_11121314",
    "bill_ref_no": "BIZ-TEST-PR05004",
    "currency_code": "SGD",
    "payee": "{{payee_payeeliquid}}",
    "items": [
        {
          "item_name": "Dell Laptop",
          "item_number": "1",
          "item_quantity": 1,
          "item_unit_price": "1000"
        },
         {
          "item_name": "Dell Monitor",
          "item_number": "1",
          "item_quantity": 1,
          "item_unit_price": "500"
        }
    "payment-methods": {
      "payment-method": [
        {
          "name": "creditcard"
        }
      ]
    }
}
```

Response 

```
{
    "service_type": "ecom",
    "amount": "1500",
    "bill_ref_no": "BIZ-TEST-PR05004",
    "currency_code": "SGD",
    "merchant_id": "MERCHANTID",
    "items": [
        {
          "item_name": "Dell Laptop",
          "item_number": "1",
          "item_quantity": 1,
          "item_unit_price": "1000"
        },
         {
          "item_name": "Dell Monitor",
          "item_number": "1",
          "item_quantity": 1,
          "item_unit_price": "500"
        }
    ],
    "payment-redirect-url": "https://hpp-liquidpay.com?transactionId=2021022510064646307617829",
    "cancel_url": "https://hpp-liquidpay.com?transactionId=transactionId=2021022510064646307617829",
    "payer_name": "Super Mario",
    "payer_email": "Test@liquidpay.com"
}
```

- Once Merchant Receive this response, he has to redirect the customer browser with payment-redirect-url field.
- Once Customer browser is redirected, we have to build a session id based on the transaction id received from the merchant, this session id should be unique for every transaction and it expires after certain time.
- Post session id created, user should see the payment checkout page as shown in the below mockup to enter the credit card details to make the payment



